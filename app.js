// Створити функцію, яка прийматиме на вхід масив і опціональний другий аргумент parent - DOM-елемент, до якого буде прикріплений список (по дефолту має бути document.body.
//     кожен із елементів масиву вивести на сторінку у вигляді пункту списку;

const arr = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];

function getFunction (arr , parent = document.body) {
    const ul = document.createElement('ul');

    parent.prepend(ul)
  
    // console.log(ul)
    arr.forEach((item) => {
        const li = document.createElement('li');
        ul.insertAdjacentHTML('beforeend',`<li>${item}</li>`)
      
        console.log(li)
        // console.log(item)
    })
}


getFunction(arr);
